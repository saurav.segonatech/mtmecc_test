@extends('layouts.app')

@section('content')
 
<nav class="navbar navbar-expand-lg navbar-light bg-dark" style="margin-top: -25px;">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{route('main')}}" style="color: #fff;"><b>MECC</b></a>
      </li>
     
    </ul>
      <form action="{{ action('AdminController@logout')}}" method="GET">
      <button class="btn btn-primary my-2 my-sm-0" type="submit">Logout</button>
    </form>
  </div>
</nav>
<br>
<br>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   <table class="table text-center table-responsive">
                        <thead>
                           <th>SNo.</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th>Company Name</th>
                           <th>Phone No.</th>
                           <th>Role</th>
                           <th>Description</th>
                           <th>Created At</th>
                           <th>Updated At</th>
                        </thead> 

                        <tbody>
                            @foreach($user as $key => $value)
                           <tr class="text-center">
                               <th>{{$key+1}}.</th>
                               <th>{{$value->name}}</th>
                               <th>{{$value->email}}</th>
                               <th>{{$value->company_name}}</th>
                               <th>{{$value->phone_no}}</th>
                               <th>{{$value->role}}</th>
                               <th>{{$value->description}}</th>
                               <th>{{$value->created_at}}</th>
                               <th>{{$value->updated_at}}</th>
                           </tr>
                           @endforeach
                        </tbody>
                   </table>
                </div>
            </div>
        </div>
    </div>
</div>


{{-- @section('styles')
    <link href="{{asset('/css/style.css')}}" />
@stop --}}
@endsection
