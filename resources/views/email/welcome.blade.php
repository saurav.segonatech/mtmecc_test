<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Alert! New registration had been done.</h2>

<div>
    <h4>Name: {{$name}}</h4>
    <h4>Email: {{$email}}</h4>
    <h4>Company Name: {{$company_name}}</h4>
    <h4>Phone No: {{$phone_no}}</h4>
    <h4>Description: {{$description}}</h4>
</div>

</body>
</html>