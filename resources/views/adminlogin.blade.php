@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                @if (count($errors)>0)
                    @foreach($errors->all() as $error)
                        <div class="alert alert-danger" role="alert">
                            {{$error}}
                        </div>
                      @endforeach
                    @endif

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                        </div>
                    @endif

                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
            <div class="card">
                <div class="card-header">{{ __('Admin Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{action('AdminController@store')}}">
                        @csrf
                           <div class="form-group row">
                               <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                               <div class="col-md-6">
                                  <input id="email" type="email" class="form-control" name="email"> 
                               </div>
                           </div>
                            <div class="form-group row">
                               <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                               <div class="col-md-6">
                                  <input id="password" type="password" class="form-control" name="password"> 
                               </div>
                           </div>
                           <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                   <button type="submit" class="btn btn-primary">
                                        Submit
                                   </button>
                                </div>
                           </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
