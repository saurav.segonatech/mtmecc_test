<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width" initial-scale="1">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/style.css">

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  <title>MECC</title>
</head>
<body>
    <header id="home">
      <nav class="navbar navbar-expand-lg navbar-light nav-color">
        <a class="navbar-brand" href="#"><img src="img/unnamed.png" style="height: 60px; width: 75px;"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item">
              <a class="nav-link text-uppercase" href="#home"><b>Home</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-uppercase" href="#about-us"><b>About Us</b></a>
            </li>
            @if(Auth::user())
            <li class="nav-item">
              <a class="nav-link text-uppercase" href="#achievement"><b>Achievement</b></a>
            </li>
            @endif
            <li class="nav-item">
              <a class="nav-link text-uppercase" href="#mission"><b>Mission & Vision</b></a>
            </li>
            <li class="nav-item">
              <a class="nav-link text-uppercase" href="#contact"><b>Contact</b></a>
            </li>
          </ul>
          <span class="navbar-text">
            {{-- @if (Route::has('login')) --}}
              <div class="top-right links">
                @auth
                  {{-- <a href="{{ url('/home') }}">Home</a> --}}
                @else
                  <a href="{{ route('login') }}" ><b>Login</b></a>

                    @if (Route::has('register'))
                      <a href="{{ route('register') }}"><b>Register</b></a>
                     @endif
                    @endauth
                </div>
            {{-- @endif --}}
          </span>
        </div> 
      </nav>
    </header>

    <div class="header-img">
      <img src="img/mteve.jpg" class="header-img">
      <h1><b>Welcome To MECC</b></h1>
    </div>

    <div class="m-gap"></div>

     <!-- About Us Section -->   
    <section class="about-us" id="about-us">
      <h2 class="text-left"><span class="underline">ABOUT US</span></h2>
      <div class="m-gap"></div>
       <div class="container">
            <div class="row">
               <div class="col-md-6 text-justify">
                  <p><b>Mt Everest Corporate Company Pvt. Ltd. (MECC)</b> was registered under the Ministry of Commerce and Industry, Government of Nepal, over a decade ago having its office in its own premises of Sanepa, Kiran Bhawan, Nepal.</p>
                  <p>MECC Private Limited is a liaison consulting firm that provides value added information and expert assistance to foreign companies who look for business prospects in Nepal. We act as a local partner in Nepal and handle; management of key issues; meetings; logistics support through a local partner/consultant. We provide solutions and specialized representation for businesses. The opportunities can include government related contracts, semi-government contracts to private sector development contracts & projects. We are always at your service and are equipped with valuable information if you are seeking an advisor that can provide you with the highest-level services.</p>
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                  Read More
                  </button>
               </div>
               <div class="col-md-6 text-justify">
               Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
               </div>
            </div>
       </div>
    </section>
     <!-- About Us Section Ends -->
    <div class="s-gap"></div>

    <!-- Achievement Section Ends -->
    @if(Auth::user())
    <section class="achievement" id="achievement">
      <div class="s-gap"></div>
      <h2 class="text-center"><span class="underline">ACHIEVEMENT</span></h2>
      <div class="s-gap"></div>
      <div class="container">
        <div class="row">
            <div class="col-md-4 achievement-left-left">
              <div class="card hero">
                <div class="card-body">
                  <h4 class="card-title">Oki Japan</h4>
                  <p class="card-text">Rural Telecommunication Tower construction project in 16 districts of Nepal under Japanese Grant.</p>
                  <a href="#" data-toggle="modal" data-target="#okijapan">Read More</a>
                </div>
              </div>
            </div>
            <div class="col-md-4 achievement-left-right">
              <div class="card hero">
                  <div class="card-body">
                    <h4 class="card-title">Gitec China</h4>
                    <p class="card-text">Field Survey and Preparation DPR of 10 MW Hydropower Project under MECC finance.</p>
                    <a href="#" data-toggle="modal" data-target="#gitecchina">Read More</a>
                  </div>
              </div>
            </div>
            <div class="col-md-4 achievement-left-right">
              <div class="card hero">
                <div class="card-body">
                  <h4 class="card-title">Schlumberger France</h4>
                  <p class="card-text">Smart Card Payphone Project in Kathmandu.</p>
                  <a href="#" data-toggle="modal" data-target="#schlumbergerfrance">Read More</a>
                </div>
              </div>
            </div>
        </div>
        
        <div class="s-gap"></div>

        <div class="row">
            <div class="col-md-4 achievement-left-left">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Ping xiang China JV with China Henan: </h4>
                  <p class="card-text">Rajapur Irrigation project in remote mid western region of Nepal, under ADB finance.</p>
                  <a href="#" data-toggle="modal" data-target="#pingxiangchina">Read More</a>
                </div>
                  </div>
            </div>
            <div class="col-md-4 achievement-left-right">
              <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Kay Bouvet Engineering Pvt.Ltd. India</h4>
                    <p class="card-text">Supply, Delivery, Erection and Commissioning of Sugar plant in Nepal, Financed by Private Entrepreneurs.</p>
                    <a href="#" data-toggle="modal" data-target="#kaybouvetengineering">Read More</a>
                  </div>
              </div>
            </div>
            <div class="col-md-4 achievement-left-right">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Himalayan Pipe Industry, Solan, HP, India</h4>
                  <p class="card-text">Supply and Delivery of HDPE pipes to Water Supply department, Nepal Govt. under ADB finance.</p>
                  <a href="#" data-toggle="modal" data-target="#himalayanpipeindustry">Read More</a>
                </div>
              </div>
            </div>
        </div>
      </div>
      <div class="m-gap"></div>
    </section>
    @endif
    <!-- Achievement Section Ends
    
    <!-- Slider Section-->
    <section class="slider">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="s-gap"></div>
              <h4 class="text-center">Ongoing Works</h4>
                <p class="text-center">102 MW Middle Bhotekoshi Hydroelectric projects under the finance of Government of Nepal.</p>
            </div>
            <div class="carousel-item">
              <div class="s-gap"></div>
                <h4 class="text-center">MECC Partners</h4>
                <p class="text-center">Promoter of Siddhartha Bank Limited. (A Commercial Bank in Kathmandu) & Promoter/Developer of 10 MW Hydropower project in Nepal.</p>
            </div>
          </div>
        </div>
    </section> 
    <!--Slider Section Ends -->

    <!-- Mission original Section  -->
    <section class="mission" id="mission">
       <div class="m-gap"></div>
       <div class="container">
            <div class="row">
              <h2 class=""><span class="underline">Mission & Vision</span></h2>
              <div class="m-gap"></div>
               <div class="col-md-6 text-justify">
             <p>MECC will continue to contribute to the development of Nepal through strategic project plan and promotion. We build value for our clients through strength of our client satisfaction and by consistently upgrading our information for superior results.</p>
             <p><b>MECC is the associate member of the following:</b></p>
                  <p>-Nepal chamber of Commerce and industry</p>
                  <p>-Federation of Nepalese Chamber of Commerce and Industry (FNCCI)</p>
               </div>
               <div class="col-md-6 text-justify">
               Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
               </div>
            </div>
            <div class="m-gap"></div>
       </div>
    </section>
    <!-- Mission original Section Ends -->

    <!-- Footer Section-->
    <footer class="footer" id="contact">
      <div class="s-gap"></div>
       <div class="container">
        <hr style="background-color: white;">
         <div class="row">
           <div class="col-md-4 footer-content text-justify">
              <p><b>Mt Everest Corporate Company Pvt. Ltd. (MECC)</b> was registered under the Ministry of Commerce and Industry, Government of Nepal, over a decade ago having its office in its own premises of Sanepa, Kiran Bhawan, Nepal.</p>
              We provide value added information and expert assistance to foreign companies who look for business prospects in Nepal.
           </div>
           <div class="col-md-1">
             
           </div>
           <div class="col-md-3 footer-support">
              <h4 class="support-text">Support</h4>
              <li><a href="service.html">Tender Assign</a></li>
                <li><a href="service.html">24/7 Support</a></li>
                <li><a href="service.html">Help Center</a></li>
                <li><a href="service.html">Terms & Condition</a></li>
                <li><a href="service.html">Private Policy</a></li>
           </div>
           <div class="col-md-4 footer-contact">
              <h4 class="contact-text">Our Contact</h4>
                <li>Mt Everest Corporate Company Pvt. Ltd. (MECC)</li>
                <li>Sanepa, Kiran Bhawan, Nepal.</li>
                <li>Phone No:- 00977-1-5554989 </li>
                <li>Mob:- 00977-9851208301/ 00977-9851205873</li>
                <li>Fax: 00977-1-5537473</li>
                <li>Email:- mteverestpower@gmail.com</li>
           </div>
         </div>
         <hr style="background-color: white;">
          <div class="xs-gap"></div>
          <div class="row text-center">
              <div class="col-md-12 footer-bottom-left" >
                <a href="https://www.facebook.com/"><i class="fab fa-facebook"></i></a>
                <a href="https://www.youtube.com/"><i class="fab fa-youtube"></i></a>
                <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
              </div>
            </div>
       </div>
           <div class="s-gap"></div>
    </footer>
    <!-- Footer Section Ends -->

    <!-- Copyright Section -->
    <div class="copyright">
      <div class="s-gap"></div>
       <p class="text-center">copyright &copy; MECC | Designed By Segonatech</p> 
      <div class="xs-gap"></div>
    </div>
    <!-- Copyright Section Ends -->
</body>

<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">ABOUT US</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p class="text-justify"><b>Mt Everest Corporate Company Pvt. Ltd. (MECC)</b> was registered under the Ministry of Commerce and Industry, Government of Nepal, over a decade ago having its office in its own premises of Sanepa, Kiran Bhawan, Nepal.</p>
        <p class="text-justify">MECC Private Limited is a liaison consulting firm that provides value added information and expert assistance to foreign companies who look for business prospects in Nepal. We act as a local partner in Nepal and handle; management of key issues; meetings; logistics support through a local partner/consultant. We provide solutions and specialized representation for businesses. The opportunities can include government related contracts, semi-government contracts to private sector development contracts& projects. We are always at your service and are equipped with valuable information if you are seeking an advisor that can provide you with the highest-level services.</p>
        <p class="text-justify">MECC has strong relationship with major local buyers, Government Ministries and Semi-Governments contacts.  MECC remains tightly focused on delivering excellence in a competitive market through constant efforts to achieve quality along with diligence and professionalism.<p>
        <p class="text-justify"><b>We are known for helping our business partners succeed</b> by offering variety of services such as logistic supports and valuable suggestions for clients of foreign companies seeking opportunities in the business arena in Nepal. Our services include contracting/executing support services, consulting services management, legal services, advisory services, financial services, human resource management, facilities management, and risk management.</p>
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

<!-- Modal -->
<div class="modal fade" id="okijapan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Oki Japan</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Rural Telecommunication Tower construction project in 16 districts of Nepal under Japanese Grant.
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

<!-- Modal -->
<div class="modal fade" id="gitecchina" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">GitecChina</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Field Survey and Preparation DPR of 10 MW Hydropower Project under MECC finance.
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

<!-- Modal -->
<div class="modal fade" id="schlumbergerfrance" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLongTitle">Schlumberger France</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         Smart Card Payphone Project in Kathmandu.
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

<!-- Modal -->
<div class="modal fade" id="pingxiangchina" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Ping xiang China JV with China Henan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Rajapur Irrigation project in remote mid western region of Nepal, under ADB finance.
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

<!-- Modal -->
<div class="modal fade" id="kaybouvetengineering" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Kay Bouvet Engineering Pvt.Ltd. India-AN ISO 9001:2000 COMPANY: </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         Supply, Delivery, Erection and Commissioning of Sugar plant in Nepal, Financed by Private Entrepreneurs.
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->

<!-- Modal -->
<div class="modal fade" id="himalayanpipeindustry" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">Himalayan Pipe Industry, Solan, HP, India</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Supply and Delivery of HDPE pipes to Water Supply department, Nepal Govt. under ADB finance.
      </div>
    </div>
  </div>
</div>
<!-- Modal Ends -->
</html>