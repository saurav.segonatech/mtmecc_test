<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class checkLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        // dd(1234);

        if(!Auth::user())
            {
                return redirect('/adminlogin');
            }
            else{
               return redirect(route('adminpanel'));
               // return $next($request); 
            }
        
    }
}
