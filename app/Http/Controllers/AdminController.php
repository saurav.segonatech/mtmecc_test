<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\User;
use Hash;
use Auth;

class AdminController extends Controller
{ 
    public function index()
    {
        return view('adminlogin');
    }

    public function store(Request $request)
    {   
    	 $matches=['email'=>$request->email];
        $users =User::where($matches)->first();
        if($users == null)
        {
           return redirect('adminlogin')->with('error', 'Username and Password doesnot match');  
        }
        else{
              // if(!Auth::attempt(['email' => $request->email, 'password' => $request->password]))
              if( ! Hash::check( $request->password,  $users->password ) )
              {
                return redirect('adminlogin')->with('error', 'Password doesnot match');
              }
              else{
                    if($users->role == 'admin')
                    { 
                        $user = User::all();
                         return view('home')->with('user', $user);
                        // return redirect(route('adminpanel'));
                    } 
                    else{
                          return redirect('adminlogin')->with('error', 'You donot have permission to access dashboard.'); 
                        }
                  }
            
             }

    }

    public function logout()
    { 
       Auth::logout();
       return redirect('adminlogin');
    }

    // public function dashboard()
    // { 
       
    // }

    public function error()
    { 
       return view('errors.error');
    }
}
