<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('main');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/adminlogin', 'AdminController@index');
Route::post('adminlogin/store', 'AdminController@store');
// Route::get('admin/dashboard', 'AdminController@dashboard')->middleware('auth')->name('adminpanel');
Route::get('/adminlogout', 'AdminController@logout')->name('adminlogout');
Route::get('/error', 'AdminController@error')->name('error');

